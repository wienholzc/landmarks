//
//  ContentView.swift
//  Landmarks
//
//  Created by Christopher Wienholz on 22/07/19.
//  Copyright © 2019 Christopher Wienholz. All rights reserved.
//

import SwiftUI

struct ContentView : View {
    var body: some View {
        Text("Hello World")
    }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
#endif
